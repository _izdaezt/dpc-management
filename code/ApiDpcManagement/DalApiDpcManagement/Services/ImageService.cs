﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;
using DalApiDpcManagement.Repositories;

namespace DalApiDpcManagement.Services
{
   public class ImageService:IImageService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ImageService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public Task<IEnumerable<Image>> GetAllWithArtist()
        {
            throw new NotImplementedException();
        }

        public Task<Image> GetMusicById(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Image>> GetMusicsByArtistId(int artistId)
        {
            throw new NotImplementedException();
        }

        public Task<Image> CreateMusic(Image newMusic)
        {
            throw new NotImplementedException();
        }

        public Task UpdateMusic(Image musicToBeUpdated, Image music)
        {
            throw new NotImplementedException();
        }

        public Task DeleteMusic(Image music)
        {
            throw new NotImplementedException();
        }
    }
}
