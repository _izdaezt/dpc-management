﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;

namespace DalApiDpcManagement.Services
{
    public  interface IImageService
    {
        Task<IEnumerable<Image>> GetAllWithArtist();
        Task<Image> GetMusicById(int id);
        Task<IEnumerable<Image>> GetMusicsByArtistId(int artistId);
        Task<Image> CreateMusic(Image newMusic);
        Task UpdateMusic(Image musicToBeUpdated, Image music);
        Task DeleteMusic(Image music);
    }
}
