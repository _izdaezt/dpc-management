﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Newtonsoft.Json;

namespace DalApiDpcManagement.ClassModel
{
    public class TaggingModel
    {
        public int Index { get; set; }
        public string Note { get; set; }
        public bool IsJapan { get; set; }
        [JsonProperty("left")]
        public float X { get; set; }
        [JsonProperty("top")]
        public float Y { get; set; }
        [JsonProperty("width")]
        public float W { get; set; }
        [JsonProperty("height")]
        public float H { get; set; }

    }
}
