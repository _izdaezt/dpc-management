﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DalApiDpcManagement.Repositories
{
    public  interface IUnitOfWork:IDisposable
    {
        IImageRepository Image { get; }
        Task<int> CommitAsync();
    }
}
