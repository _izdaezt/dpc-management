﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;

namespace DalApiDpcManagement.Repositories
{
    public interface IImageRepository : IRepository<Image>
    {


        Task<IEnumerable<Image>> GetAllWithArtistAsync();
        Task<Image> GetWithArtistByIdAsync(int id);
        Task<IEnumerable<Image>> GetAllWithArtistByArtistIdAsync(int artistId);
    }
}
