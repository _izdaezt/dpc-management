﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;
using DalApiDpcManagement.Repositories;

namespace DalApiDpcManagement
{
    public class UnitOfWork:IUnitOfWork
    {
        private readonly DpcEntryContext _context;
        private ImageRepository _imageRepository;

        public UnitOfWork(DpcEntryContext context)
        {
            this._context = context;
        }
        public IImageRepository Image => _imageRepository ??= new ImageRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
