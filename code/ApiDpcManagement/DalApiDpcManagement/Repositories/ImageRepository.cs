﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualBasic;

namespace DalApiDpcManagement.Repositories
{
    public class ImageRepository:Repository<Image>,IImageRepository
    {
        public ImageRepository(DpcEntryContext context) : base(context)
        {
        }
        private DpcEntryContext _context => Context as DpcEntryContext;

        public  async Task<IEnumerable<Image>> GetAllWithArtistAsync()
        {
            var listImage = await _context.Image.ToListAsync();
            return listImage;
        }

        public Task<Image> GetWithArtistByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Image>> GetAllWithArtistByArtistIdAsync(int artistId)
        {
            throw new NotImplementedException();
        }
    }
}
