﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace DalApiDpcManagement.Models
{
    public partial class DpcEntryContext : DbContext
    {
        public DpcEntryContext()
        {
        }

        public DpcEntryContext(DbContextOptions<DpcEntryContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<AccountRole> AccountRole { get; set; }
        public virtual DbSet<Image> Image { get; set; }
        public virtual DbSet<ImageTagEntry> ImageTagEntry { get; set; }
        public virtual DbSet<ImageTask> ImageTask { get; set; }
        public virtual DbSet<Package> Package { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<TaskLevel> TaskLevel { get; set; }
        public virtual DbSet<Tasks> Tasks { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=;port=33063306;database=DpcEntry;User ID=root;password=Hacking12345");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.HasKey(e => e.AccId);

                entity.HasComment("Bảng chứa thông tin user được map với user của hệ thông DTMS");

                entity.Property(e => e.AccName).HasMaxLength(50);

                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.FullName).HasMaxLength(500);

                entity.Property(e => e.PathAvatar).HasMaxLength(500);
            });

            modelBuilder.Entity<AccountRole>(entity =>
            {
                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.HasOne(d => d.Acc)
                    .WithMany(p => p.AccountRole)
                    .HasForeignKey(d => d.AccId)
                    .HasConstraintName("FK_AccountRole_Account");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AccountRole)
                    .HasForeignKey(d => d.RoleId)
                    .HasConstraintName("FK_AccountRole_Role");
            });

            modelBuilder.Entity<Image>(entity =>
            {
                entity.Property(e => e.ContentTask).HasMaxLength(50);

                entity.Property(e => e.ImageName).HasMaxLength(500);

                entity.Property(e => e.ImagePath).HasMaxLength(500);

                entity.HasOne(d => d.Package)
                    .WithMany(p => p.Image)
                    .HasForeignKey(d => d.PackageId)
                    .HasConstraintName("FK_Image_Package");
            });

            modelBuilder.Entity<ImageTagEntry>(entity =>
            {
                entity.HasKey(e => e.ImageId);

                entity.HasComment("Bảng lưu dữ liệu của MDGC tag");

                entity.Property(e => e.ImageId).ValueGeneratedNever();

                entity.Property(e => e.AccName).HasMaxLength(50);

                entity.Property(e => e.Duration).HasComment("đơn vị giây");

                entity.HasOne(d => d.Image)
                    .WithOne(p => p.ImageTagEntry)
                    .HasForeignKey<ImageTagEntry>(d => d.ImageId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ImageTagEntry_Image");
            });

            modelBuilder.Entity<ImageTask>(entity =>
            {
                entity.Property(e => e.AccName).HasMaxLength(50);

                entity.HasOne(d => d.Image)
                    .WithMany(p => p.ImageTask)
                    .HasForeignKey(d => d.ImageId)
                    .HasConstraintName("FK_ImageTask_Image");

                entity.HasOne(d => d.Task)
                    .WithMany(p => p.ImageTask)
                    .HasForeignKey(d => d.TaskId)
                    .HasConstraintName("FK_ImageTask_Task");
            });

            modelBuilder.Entity<Package>(entity =>
            {
                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.FilePath).HasMaxLength(500);

                entity.Property(e => e.PackageName).HasMaxLength(500);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.Package)
                    .HasForeignKey(d => d.ProjectId)
                    .HasConstraintName("FK_Package_Project");
            });

            modelBuilder.Entity<PurchaseOrder>(entity =>
            {
                entity.HasKey(e => e.PoId)
                    .HasName("PK_Project");

                entity.HasComment("Tên các dự án nghiệp vụ riêng biệt");

                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.OrderName).HasMaxLength(50);
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.RoleName).HasMaxLength(50);
            });

            modelBuilder.Entity<TaskLevel>(entity =>
            {
                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.Description).HasMaxLength(500);

                entity.Property(e => e.LevelName).HasMaxLength(50);

                entity.Property(e => e.TimeDeadLine).HasComment("tiêu chuẩn deadline của mỗi level");
            });

            modelBuilder.Entity<Tasks>(entity =>
            {
                entity.HasKey(e => e.TaskId)
                    .HasName("PK_Task");

                entity.Property(e => e.AccCheck).HasMaxLength(50);

                entity.Property(e => e.AccEntry).HasMaxLength(50);

                entity.Property(e => e.AccOp).HasMaxLength(50);

                entity.Property(e => e.AccUpdate).HasMaxLength(50);

                entity.Property(e => e.Status).HasMaxLength(50);

                entity.Property(e => e.StatusCheck).HasMaxLength(50);

                entity.Property(e => e.StatusEntry).HasMaxLength(50);

                entity.Property(e => e.StatusOp).HasMaxLength(50);

                entity.Property(e => e.TaskName).HasMaxLength(500);

                entity.HasOne(d => d.TaskLevel)
                    .WithMany(p => p.Tasks)
                    .HasForeignKey(d => d.TaskLevelId)
                    .HasConstraintName("FK_Task_TaskLevel");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
