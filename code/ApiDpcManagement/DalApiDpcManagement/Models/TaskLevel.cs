﻿using System;
using System.Collections.Generic;

namespace DalApiDpcManagement.Models
{
    public partial class TaskLevel
    {
        public TaskLevel()
        {
            Tasks = new HashSet<Tasks>();
        }

        public long TaskLevelId { get; set; }
        public string LevelName { get; set; }
        public int? TimeDeadLine { get; set; }
        public string Description { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AccUpdate { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Tasks> Tasks { get; set; }
    }
}
