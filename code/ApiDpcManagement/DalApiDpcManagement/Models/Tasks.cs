﻿using System;
using System.Collections.Generic;

namespace DalApiDpcManagement.Models
{
    public partial class Tasks
    {
        public Tasks()
        {
            ImageTask = new HashSet<ImageTask>();
        }

        public long TaskId { get; set; }
        public long? PackageId { get; set; }
        public long? TaskLevelId { get; set; }
        public string TaskName { get; set; }
        public string Status { get; set; }
        public string StatusEntry { get; set; }
        public string AccEntry { get; set; }
        public string StatusOp { get; set; }
        public string AccOp { get; set; }
        public string StatusCheck { get; set; }
        public string AccCheck { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateDeadLine { get; set; }
        public string AccUpdate { get; set; }

        public virtual TaskLevel TaskLevel { get; set; }
        public virtual ICollection<ImageTask> ImageTask { get; set; }
    }
}
