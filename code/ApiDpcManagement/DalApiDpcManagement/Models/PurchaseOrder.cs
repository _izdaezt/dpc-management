﻿using System;
using System.Collections.Generic;

namespace DalApiDpcManagement.Models
{
    public partial class PurchaseOrder
    {
        public PurchaseOrder()
        {
            Package = new HashSet<Package>();
        }

        public long PoId { get; set; }
        public string OrderName { get; set; }
        public string Description { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AccUpdate { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<Package> Package { get; set; }
    }
}
