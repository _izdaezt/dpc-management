﻿using System;
using System.Collections.Generic;

namespace DalApiDpcManagement.Models
{
    public partial class ImageTask
    {
        public long Id { get; set; }
        public long? ImageId { get; set; }
        public long? TaskId { get; set; }
        public string AccName { get; set; }
        public DateTime? DateCreate { get; set; }

        public virtual Image Image { get; set; }
        public virtual Tasks Task { get; set; }
    }
}
