﻿// <auto-generated />
using System;
using DalApiDpcManagement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace DalApiDpcManagement.Migrations
{
    [DbContext(typeof(DpcEntryContext))]
    partial class DpcEntryContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("DalApiDpcManagement.Models.Account", b =>
                {
                    b.Property<long>("AccId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("FullName")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<bool?>("IsDelete")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("PassWord")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("PathAvatar")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.HasKey("AccId");

                    b.ToTable("Account");

                    b.HasComment("Bảng chứa thông tin user được map với user của hệ thông DTMS");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.AccountRole", b =>
                {
                    b.Property<long>("AccountRoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<long?>("AccId")
                        .HasColumnType("bigint");

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<long?>("RoleId")
                        .HasColumnType("bigint");

                    b.HasKey("AccountRoleId");

                    b.HasIndex("AccId");

                    b.HasIndex("RoleId");

                    b.ToTable("AccountRole");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Image", b =>
                {
                    b.Property<long>("ImageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("ContentTask")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("ImageName")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<string>("ImagePath")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<long?>("PackageId")
                        .HasColumnType("bigint");

                    b.HasKey("ImageId");

                    b.HasIndex("PackageId");

                    b.ToTable("Image");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.ImageTagEntry", b =>
                {
                    b.Property<long>("ImageId")
                        .HasColumnType("bigint");

                    b.Property<string>("AccName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<int?>("Duration")
                        .HasColumnType("int")
                        .HasComment("đơn vị giây");

                    b.Property<string>("Value")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("ImageId");

                    b.ToTable("ImageTagEntry");

                    b.HasComment("Bảng lưu dữ liệu của MDGC tag");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.ImageTask", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<long?>("ImageId")
                        .HasColumnType("bigint");

                    b.Property<long?>("TaskId")
                        .HasColumnType("bigint");

                    b.HasKey("Id");

                    b.HasIndex("ImageId");

                    b.HasIndex("TaskId");

                    b.ToTable("ImageTask");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Package", b =>
                {
                    b.Property<long>("PackageId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateDeadLine")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<string>("FilePath")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<bool?>("IsDelete")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("PackageName")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<long?>("ProjectId")
                        .HasColumnType("bigint");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.HasKey("PackageId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Package");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.PurchaseOrder", b =>
                {
                    b.Property<long>("PoId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<bool?>("IsDelete")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("OrderName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.HasKey("PoId")
                        .HasName("PK_Project");

                    b.ToTable("PurchaseOrder");

                    b.HasComment("Tên các dự án nghiệp vụ riêng biệt");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Role", b =>
                {
                    b.Property<long>("RoleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<bool?>("IsDelete")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("RoleName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.HasKey("RoleId");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.TaskLevel", b =>
                {
                    b.Property<long>("TaskLevelId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateUpdate")
                        .HasColumnType("datetime(6)");

                    b.Property<string>("Description")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.Property<bool?>("IsDelete")
                        .HasColumnType("tinyint(1)");

                    b.Property<string>("LevelName")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<int?>("TimeDeadLine")
                        .HasColumnType("int")
                        .HasComment("tiêu chuẩn deadline của mỗi level");

                    b.HasKey("TaskLevelId");

                    b.ToTable("TaskLevel");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Tasks", b =>
                {
                    b.Property<long>("TaskId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    b.Property<string>("AccCheck")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("AccEntry")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("AccOp")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("AccUpdate")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<DateTime?>("DateCreate")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime?>("DateDeadLine")
                        .HasColumnType("datetime(6)");

                    b.Property<long?>("PackageId")
                        .HasColumnType("bigint");

                    b.Property<string>("Status")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("StatusCheck")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("StatusEntry")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<string>("StatusOp")
                        .HasColumnType("varchar(50) CHARACTER SET utf8mb4")
                        .HasMaxLength(50);

                    b.Property<long?>("TaskLevelId")
                        .HasColumnType("bigint");

                    b.Property<string>("TaskName")
                        .HasColumnType("varchar(500) CHARACTER SET utf8mb4")
                        .HasMaxLength(500);

                    b.HasKey("TaskId")
                        .HasName("PK_Task");

                    b.HasIndex("TaskLevelId");

                    b.ToTable("Tasks");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.AccountRole", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.Account", "Acc")
                        .WithMany("AccountRole")
                        .HasForeignKey("AccId")
                        .HasConstraintName("FK_AccountRole_Account");

                    b.HasOne("DalApiDpcManagement.Models.Role", "Role")
                        .WithMany("AccountRole")
                        .HasForeignKey("RoleId")
                        .HasConstraintName("FK_AccountRole_Role");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Image", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.Package", "Package")
                        .WithMany("Image")
                        .HasForeignKey("PackageId")
                        .HasConstraintName("FK_Image_Package");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.ImageTagEntry", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.Image", "Image")
                        .WithOne("ImageTagEntry")
                        .HasForeignKey("DalApiDpcManagement.Models.ImageTagEntry", "ImageId")
                        .HasConstraintName("FK_ImageTagEntry_Image")
                        .IsRequired();
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.ImageTask", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.Image", "Image")
                        .WithMany("ImageTask")
                        .HasForeignKey("ImageId")
                        .HasConstraintName("FK_ImageTask_Image");

                    b.HasOne("DalApiDpcManagement.Models.Tasks", "Task")
                        .WithMany("ImageTask")
                        .HasForeignKey("TaskId")
                        .HasConstraintName("FK_ImageTask_Task");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Package", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.PurchaseOrder", "Project")
                        .WithMany("Package")
                        .HasForeignKey("ProjectId")
                        .HasConstraintName("FK_Package_Project");
                });

            modelBuilder.Entity("DalApiDpcManagement.Models.Tasks", b =>
                {
                    b.HasOne("DalApiDpcManagement.Models.TaskLevel", "TaskLevel")
                        .WithMany("Tasks")
                        .HasForeignKey("TaskLevelId")
                        .HasConstraintName("FK_Task_TaskLevel");
                });
#pragma warning restore 612, 618
        }
    }
}
