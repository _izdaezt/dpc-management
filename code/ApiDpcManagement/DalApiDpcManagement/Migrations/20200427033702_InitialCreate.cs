﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DalApiDpcManagement.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Account",
                columns: table => new
                {
                    AccId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AccName = table.Column<string>(maxLength: 50, nullable: true),
                    FullName = table.Column<string>(maxLength: 500, nullable: true),
                    PathAvatar = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateUpdate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true),
                    PassWord = table.Column<string>(nullable: true),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Account", x => x.AccId);
                },
                comment: "Bảng chứa thông tin user được map với user của hệ thông DTMS");

            migrationBuilder.CreateTable(
                name: "PurchaseOrder",
                columns: table => new
                {
                    PoId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    OrderName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateUpdate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Project", x => x.PoId);
                },
                comment: "Tên các dự án nghiệp vụ riêng biệt");

            migrationBuilder.CreateTable(
                name: "Role",
                columns: table => new
                {
                    RoleId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RoleName = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateUpdate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Role", x => x.RoleId);
                });

            migrationBuilder.CreateTable(
                name: "TaskLevel",
                columns: table => new
                {
                    TaskLevelId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    LevelName = table.Column<string>(maxLength: 50, nullable: true),
                    TimeDeadLine = table.Column<int>(nullable: true, comment: "tiêu chuẩn deadline của mỗi level"),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateUpdate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaskLevel", x => x.TaskLevelId);
                });

            migrationBuilder.CreateTable(
                name: "Package",
                columns: table => new
                {
                    PackageId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<long>(nullable: true),
                    PackageName = table.Column<string>(maxLength: 500, nullable: true),
                    FilePath = table.Column<string>(maxLength: 500, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateDeadLine = table.Column<DateTime>(nullable: true),
                    DateUpdate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true),
                    IsDelete = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Package", x => x.PackageId);
                    table.ForeignKey(
                        name: "FK_Package_Project",
                        column: x => x.ProjectId,
                        principalTable: "PurchaseOrder",
                        principalColumn: "PoId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AccountRole",
                columns: table => new
                {
                    AccountRoleId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AccId = table.Column<long>(nullable: true),
                    RoleId = table.Column<long>(nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountRole", x => x.AccountRoleId);
                    table.ForeignKey(
                        name: "FK_AccountRole_Account",
                        column: x => x.AccId,
                        principalTable: "Account",
                        principalColumn: "AccId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AccountRole_Role",
                        column: x => x.RoleId,
                        principalTable: "Role",
                        principalColumn: "RoleId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    TaskId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    PackageId = table.Column<long>(nullable: true),
                    TaskLevelId = table.Column<long>(nullable: true),
                    TaskName = table.Column<string>(maxLength: 500, nullable: true),
                    Status = table.Column<string>(maxLength: 50, nullable: true),
                    StatusEntry = table.Column<string>(maxLength: 50, nullable: true),
                    AccEntry = table.Column<string>(maxLength: 50, nullable: true),
                    StatusOp = table.Column<string>(maxLength: 50, nullable: true),
                    AccOp = table.Column<string>(maxLength: 50, nullable: true),
                    StatusCheck = table.Column<string>(maxLength: 50, nullable: true),
                    AccCheck = table.Column<string>(maxLength: 50, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    DateDeadLine = table.Column<DateTime>(nullable: true),
                    AccUpdate = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Task", x => x.TaskId);
                    table.ForeignKey(
                        name: "FK_Task_TaskLevel",
                        column: x => x.TaskLevelId,
                        principalTable: "TaskLevel",
                        principalColumn: "TaskLevelId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Image",
                columns: table => new
                {
                    ImageId = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    PackageId = table.Column<long>(nullable: true),
                    ImageName = table.Column<string>(maxLength: 500, nullable: true),
                    ImagePath = table.Column<string>(maxLength: 500, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true),
                    ContentTask = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Image", x => x.ImageId);
                    table.ForeignKey(
                        name: "FK_Image_Package",
                        column: x => x.PackageId,
                        principalTable: "Package",
                        principalColumn: "PackageId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ImageTagEntry",
                columns: table => new
                {
                    ImageId = table.Column<long>(nullable: false),
                    AccName = table.Column<string>(maxLength: 50, nullable: true),
                    Value = table.Column<string>(nullable: true),
                    Duration = table.Column<int>(nullable: true, comment: "đơn vị giây"),
                    DateCreate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageTagEntry", x => x.ImageId);
                    table.ForeignKey(
                        name: "FK_ImageTagEntry_Image",
                        column: x => x.ImageId,
                        principalTable: "Image",
                        principalColumn: "ImageId",
                        onDelete: ReferentialAction.Restrict);
                },
                comment: "Bảng lưu dữ liệu của MDGC tag");

            migrationBuilder.CreateTable(
                name: "ImageTask",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ImageId = table.Column<long>(nullable: true),
                    TaskId = table.Column<long>(nullable: true),
                    AccName = table.Column<string>(maxLength: 50, nullable: true),
                    DateCreate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImageTask", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ImageTask_Image",
                        column: x => x.ImageId,
                        principalTable: "Image",
                        principalColumn: "ImageId",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ImageTask_Task",
                        column: x => x.TaskId,
                        principalTable: "Tasks",
                        principalColumn: "TaskId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountRole_AccId",
                table: "AccountRole",
                column: "AccId");

            migrationBuilder.CreateIndex(
                name: "IX_AccountRole_RoleId",
                table: "AccountRole",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_Image_PackageId",
                table: "Image",
                column: "PackageId");

            migrationBuilder.CreateIndex(
                name: "IX_ImageTask_ImageId",
                table: "ImageTask",
                column: "ImageId");

            migrationBuilder.CreateIndex(
                name: "IX_ImageTask_TaskId",
                table: "ImageTask",
                column: "TaskId");

            migrationBuilder.CreateIndex(
                name: "IX_Package_ProjectId",
                table: "Package",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_TaskLevelId",
                table: "Tasks",
                column: "TaskLevelId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountRole");

            migrationBuilder.DropTable(
                name: "ImageTagEntry");

            migrationBuilder.DropTable(
                name: "ImageTask");

            migrationBuilder.DropTable(
                name: "Account");

            migrationBuilder.DropTable(
                name: "Role");

            migrationBuilder.DropTable(
                name: "Image");

            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Package");

            migrationBuilder.DropTable(
                name: "TaskLevel");

            migrationBuilder.DropTable(
                name: "PurchaseOrder");
        }
    }
}
