﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ApiDpcManagement.Models;
using DalApiDpcManagement;
using DalApiDpcManagement.Models;
using DalApiDpcManagement.Repositories;
using DalApiDpcManagement.Services;
using Microsoft.Extensions.Configuration;

namespace ApiDpcManagement.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
         private readonly IUnitOfWork _unit;
        private readonly IConfiguration _config;
        private readonly IImageService _imageService;

        public HomeController(IImageService imageService,IUnitOfWork unit,DpcEntryContext context, IConfiguration config,ILogger<HomeController> logger)
        {
            _unit = unit;
            _context = context;
            _config = config;
            _logger = logger;
            _imageService = imageService;

        }

        public async Task<IActionResult> Index()
        {
            //var ro = await _unit.Image.GetAllWithArtistAsync();
          
            var role = _context.Role.ToList();
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
