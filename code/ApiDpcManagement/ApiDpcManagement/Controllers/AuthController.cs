﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ApiDpcManagement.Models;
using BPOApiClient.Core;
using DalApiDpcManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Account = DalApiDpcManagement.Models.Account;

namespace ApiDpcManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
        private readonly IConfiguration _config;
          private BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();
        public AuthController(DpcEntryContext context, IConfiguration config, ILogger<HomeController> logger)
        {
            _context = context;
            _config = config;
            _logger = logger;
        }
        /// <summary>
        /// validate tài khoản vs TM
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost("GetToken")]
        public async Task<IActionResult> Authenticate(Account account)
        {
            try
            {
                //return BadRequest("123124124124");
                var pathServerTm = _config["AppSettings:ServerTM"];
                apiClient.SetAddress(pathServerTm);
                //đăng nhập vào hệ thống TM
                var response = apiClient.PostPrc(
                                    "api/external/authenticate",
                                      new UserTm()
                                      {
                                          Username = account.AccName,
                                          Password = account.PassWord,
                                           RememberMe = true
                                      });
                if (response.status)
                {
                    //check trong user training có chưa
                    var loginToken = JsonConvert.DeserializeObject<LoginToken>(response.result);
                    var _acc = await _context.Account.SingleOrDefaultAsync(a => a.AccName == loginToken.Login);
                    if (_acc == null)//tạo mới
                    {
                        _acc = new Account()
                        {
                            AccName = loginToken.Login,
                            DateCreate = DateTime.UtcNow,
                            IsDelete = false,
                        };
                        await _context.Account.AddAsync(_acc);
                        await _context.SaveChangesAsync();
                    }
                    //đăng ký token và authen
                    //generate token

                    var tokenHandler = new JwtSecurityTokenHandler();
                    var AppSettingstoken = _config["AppSettings:Token"];
                    var key = Encoding.ASCII.GetBytes(AppSettingstoken);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]{
                                new Claim(ClaimTypes.NameIdentifier,_acc.AccId.ToString()),
                                new Claim(ClaimTypes.Name, _acc.AccName),
                                new Claim(ClaimTypes.Role, "")
                            }),
                        Expires = DateTime.Now.AddDays(1),
                        SigningCredentials = new SigningCredentials(
                                        new SymmetricSecurityKey(key),
                                        SecurityAlgorithms.HmacSha512Signature),
                        
                        
                    };

                    var token = tokenHandler.CreateToken(tokenDescriptor);
                    loginToken.IdTokenManagement = tokenHandler.WriteToken(token);
                    loginToken.AccId = _acc.AccId;
                    return Ok(loginToken);
                }
                else
                {
                    return Unauthorized(response.result);
                }
                
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }

        }

    }

   
}