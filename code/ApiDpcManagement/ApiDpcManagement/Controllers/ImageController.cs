﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiDpcManagement.Models;
using DalApiDpcManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace ApiDpcManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
        private readonly IConfiguration _config;
        private BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();
        public ImageController(DpcEntryContext context, IConfiguration config, ILogger<HomeController> logger)
        {
            _context = context;
            _config = config;
            _logger = logger;
        }

        public async Task<IActionResult> Create()
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }
        public async Task<IActionResult> Create([FromForm]string strImageObject, IFormFile file)
        {
            try
            {
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }

    }
}