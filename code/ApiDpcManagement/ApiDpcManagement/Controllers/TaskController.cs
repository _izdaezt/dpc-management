﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ApiDpcManagement.Models;
using DalApiDpcManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ServiceHelper.Service;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Parsing;
using Tasks = DalApiDpcManagement.Models.Tasks;

namespace ApiDpcManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
        private readonly IConfiguration _config;

        public TaskController(DpcEntryContext context, IConfiguration config, ILogger<HomeController> logger)
        {
            _context = context;
            _config = config;
            _logger = logger;

        }
        /// <summary>
        /// xem list all task 
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Read()
        {
            try
            {
                var tasks = await _context.Tasks.ToListAsync();
                return Ok(tasks);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }

        [HttpGet("ViewTaskMdgc/{accName}")]
        public async Task<IActionResult> ViewTaskMdgc(string accName)
        {
            try
            {
                var acc = await _context.Account.FirstOrDefaultAsync(x => x.AccName == accName);
                var taskGet = await _context.Tasks.Where(x => x.AccEntry == acc.AccName).ToListAsync();
                return Ok(taskGet);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }

        }
        /// <summary>
        /// Xem list task từ package id
        /// </summary>
        /// <param name="packageId"></param>
        /// <returns></returns>
        [HttpGet("Package/{packageId}")]
        public async Task<IActionResult> Read(long packageId)
        {
            try
            {

                var tasks = await _context.Tasks.Where(x => x.PackageId == packageId)
                                    .Include(x => x.ImageTask)
                                    .ThenInclude(x => x.Image)
                                    .ToListAsync();
                return Ok(tasks);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }

       


       
        #region registry, start, pending, submit  task
        /// <summary>
        /// mdgc vào nhận task
        /// tìm tới task chưa có người nhận và fill tên vào
        /// </summary>
        /// <param name="accName"></param>
        /// <returns></returns>
        [HttpGet("RegistryMdgc/{accName}")]
        public async Task<IActionResult> RegistryMdgc(string accName)
        {
            try
            {
                var acc = await _context.Account.FirstOrDefaultAsync(x => x.AccName == accName);

                var taskOpen = await _context.Tasks.FirstOrDefaultAsync(x => x.AccEntry == null);
                var package = await _context.Package.FirstOrDefaultAsync(x => x.PackageId == taskOpen.PackageId);
                taskOpen.AccEntry = acc.AccName;
                //taskOpen.Status = StatusTask.Doing.ToString();
                // taskOpen.StatusEntry = StatusTask.Doing.ToString();
                package.Status = StatusPackage.Doing.ToString();
                await _context.SaveChangesAsync();
                return Ok(taskOpen);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }

        }
        /// <summary>
        /// Lấy thông tin task từ taskid
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet("GetTask/{taskId}")]
        public async Task<IActionResult> StartTask(long taskId)
        {
            var task = await _context.Tasks.Where(x => x.TaskId == taskId)
                                                    .Include(x => x.ImageTask)
                                                    .ThenInclude(x => x.Image)
                                                    .ThenInclude(x => x.ImageTagEntry)
                                                    .FirstOrDefaultAsync();
            task.StatusEntry = task.StatusEntry==StatusTask.Done.ToString()?task.StatusEntry:StatusTask.Doing.ToString();
            task.Status =task.Status==StatusTask.Done.ToString()?task.Status:StatusTask.Doing.ToString();
            await _context.SaveChangesAsync();
            return Ok(task);
        }
        /// <summary>
        /// Submit tagging=> update trạng thái task
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        [HttpPost("SubmitTag/{taskId}")]
        public async Task<IActionResult> CompleteMDGCTask(List<ImageTagEntry> listImageTag,long taskId)
        {
            try
            {
                if (listImageTag.Count<1)
                {
                    return BadRequest("Non object tag!");
                }
                //lấy thông tin task
                var task =await _context.Tasks.FirstOrDefaultAsync(x => x.TaskId == taskId);
                //submit Image
                foreach (var imageTag in listImageTag)
                {
                    try
                    {
                        await _context.ImageTagEntry.AddAsync(imageTag);
                    }
                    catch (Exception e)// try catch cho trường hợp ảnh chung của task, thằng nào submit sau sẽ update nội dung lên
                    {
                        var imageTagTemp = await _context.ImageTagEntry
                                .FirstOrDefaultAsync(x => x.ImageId == imageTag.ImageId);
                        if (imageTagTemp!=null)
                        {
                            _context.ImageTagEntry.Update(imageTag);
                            //imageTagTemp.AccName = imageTag.AccName;
                            //imageTagTemp.Value = imageTag.Value;
                            //imageTagTemp.Duration = imageTag.Duration;
                            //imageTagTemp.DateCreate = imageTag.DateCreate;
                            await _context.SaveChangesAsync();
                        }
                    }
                }
                task.StatusEntry = StatusTask.Done.ToString();
                var update = _context.Tasks.Update(task);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }

        #endregion

        #region export task to pdf
        [HttpGet("ExportPdfNote")]
        public async Task<FileResult> ExportPdfDemo()
        {
            //Create a new PDF document.
            PdfDocument document = new PdfDocument();
            //Add a page to the document.
            PdfPage page = document.Pages.Add();
            //Create PDF graphics for the page.
            PdfGraphics graphics = page.Graphics;
            //Set the standard font.
            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 20);
            //Draw the text.
            graphics.DrawString("Hello World!!!", font, PdfBrushes.Black, new Syncfusion.Drawing.PointF(0, 0));
            //Creating the stream object
            MemoryStream stream = new MemoryStream();
            //Save the document into memory stream
            document.Save(stream);
            //If the position is not set to '0' then the PDF will be empty.
            stream.Position = 0;
            //Close the document.
            //document.Close(true);
            //Defining the ContentType for pdf file.
            //Define the file name.
            string fileName = "Output.pdf";
            //Creates a FileContentResult object by using the file contents, content type, and file name.
            string contentType = "application/pdf";
            return File(stream, contentType, $"output.pdf");

        }
        /// <summary>
        /// Export pdf kèm note
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet("ExportPdfNote/{taskId}")]
        public async Task<FileResult> ExportPdfNote(long taskId)
        {
            var task = await _context.Tasks.Where(x => x.TaskId == taskId)
                                                    .Include(x => x.ImageTask)
                                                    .ThenInclude(x => x.Image)
                                                    .ThenInclude(x => x.ImageTagEntry)
                                                    .FirstOrDefaultAsync();
            var package = await _context.Package.FirstOrDefaultAsync(x => x.PackageId == task.PackageId);

            var pathLocalFolder = _config["AppSettings:PathUpFilePdf"];
            PdfService pdfService = new PdfService();
            var pdfStream = pdfService.ImageToPdfNote(task, pathLocalFolder);
            string contentType = "application/pdf";
            return File(pdfStream, contentType, $"{package.PackageName}-{task.TaskName}-Note.pdf");

        }
        /// <summary>
        /// export pdf kèm tag
        /// </summary>
        /// <param name="taskId"></param>
        /// <returns></returns>
        [HttpGet("ExportPdfTag/{taskId}")]
        public async Task<FileResult> ExportPdfTag(long taskId)
        {
            var task = await _context.Tasks.Where(x => x.TaskId == taskId)
                                                    .Include(x => x.ImageTask)
                                                    .ThenInclude(x => x.Image)
                                                    .ThenInclude(x => x.ImageTagEntry)
                                                    .FirstOrDefaultAsync();
            var package = await _context.Package.FirstOrDefaultAsync(x => x.PackageId == task.PackageId);

            var pathLocalFolder = _config["AppSettings:PathUpFilePdf"];
            PdfService pdfService = new PdfService();
            var pdfStream = pdfService.ImageToPdfTag(task, pathLocalFolder);
            string contentType = "application/pdf";
            return File(pdfStream, contentType, $"{package.PackageName}-{task.TaskName}-Tag.pdf");

        }
        #endregion

    }
}