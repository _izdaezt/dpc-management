﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using ServiceHelper.Service;
using Image = DalApiDpcManagement.Models.Image;
using Package = DalApiDpcManagement.Models.Package;

namespace ApiDpcManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PackageController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
        private readonly IConfiguration _config;
        private BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();
        public PackageController(DpcEntryContext context, IConfiguration config, ILogger<HomeController> logger)
        {
            _context = context;
            _config = config;
            _logger = logger;
        }
        /// <summary>
        /// view list package
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Read()
        {
            try
            {
                var packages = await _context.Package.ToListAsync();
                return Ok(packages);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }

        }
        /// <summary>
        /// đăng ký file pdf lên package
        /// </summary>
        /// <param name="strPackage"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromForm]string strPackage, IFormFile file)
        {
            try
            {
                Package package = JsonConvert.DeserializeObject<Package>(strPackage);
                string pathUpload = _config["AppSettings:PathUpFilePdf"];
                string pathSave = Path.Combine(pathUpload, package.PackageName);
                if (!Directory.Exists(pathSave))
                {
                    Directory.CreateDirectory(pathSave);
                }


                string pathFile = Path.Combine(pathSave, $"{file.FileName}");
                int renameCount = 0;
                while (System.IO.File.Exists(pathFile))
                {
                    renameCount++;
                    pathFile = Path.Combine(pathSave, $"{Path.GetFileNameWithoutExtension(file.FileName)}-({renameCount}){Path.GetExtension(file.FileName)}");
                }

                if (file.Length > 0)
                {
                    using (var stream = new FileStream(pathFile, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                        package.FilePath =pathFile.Replace(pathUpload,"");
                        package.Status = StatusPackage.Uploaded.ToString();
                        package.IsDelete = false;
                        package.AccUpdate = "";
                        package.DateCreate = DateTime.Now;
                        package.DateUpdate = DateTime.Now;
                        await _context.Package.AddAsync(package);
                        await _context.SaveChangesAsync();
                    }
                    return Ok(package);
                }
                else
                {
                    return BadRequest("File not found!");
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex.ToString());
            }
        }

        /// <summary>
        /// Extract pdf=> image
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        [HttpPost("Export")]
        public async Task<IActionResult> ExportImage(Package package)
        {
            try
            {
                var tempPackage = await _context.Package.FirstOrDefaultAsync(x => x.PackageId == package.PackageId);
                 string pathUpload = _config["AppSettings:PathUpFilePdf"];
                ServiceHelper.Service.PdfService service = new PdfService();
                var pathFileLocal = Path.Combine(pathUpload, tempPackage.FilePath);
                List<string> imagePathList = service.ToImage(pathFileLocal);
                var listImageObject = imagePathList.Select(x => new Image()
                {
                    PackageId = tempPackage.PackageId,
                    ImageName = Path.GetFileName(x),
                    ImagePath = x.Replace(pathUpload,""),
                    DateCreate = DateTime.Now
                }).ToList();

                await _context.Image.AddRangeAsync(listImageObject);
                tempPackage.Status = StatusPackage.Tasking.ToString();
                _context.Package.Update(tempPackage);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }

        }

        /// <summary>
        /// get all image của package
        /// </summary>
        /// <param name="package"></param>
        /// <returns></returns>
        [HttpGet("ReadImage")]
        public async Task<IActionResult> ReadImage(int packageId)
        {
            try
            {
                var listImage = await _context.Image.Where(x => x.PackageId == packageId).ToListAsync();
                return Ok(listImage);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }
    }
}