﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DalApiDpcManagement.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Image = DalApiDpcManagement.Models.Image;
using ImageTask = DalApiDpcManagement.Models.ImageTask;
using Tasks = DalApiDpcManagement.Models.Tasks;

namespace ApiDpcManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageTaskController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;
        private readonly DpcEntryContext _context;
        private readonly IConfiguration _config;

        public ImageTaskController(DpcEntryContext context, IConfiguration config, ILogger<HomeController> logger)
        {
            _context = context;
            _config = config;
            _logger = logger;

        }
        /// <summary>
        /// gắn task cho image
        /// </summary>
        /// <param name="imageTask"></param>
        /// <returns></returns>
        [HttpPost("Create")]
        public async Task<IActionResult> Create(List<Image> listImage)
        {
            try
            {
                if (!listImage.Any())
                {
                    return BadRequest("Image count 0");
                }
                var packageId = listImage[0].PackageId;
                var package = await _context.Package.FirstOrDefaultAsync(x => x.PackageId == packageId);
                var tempListImage = await _context.Image.Where(x => x.PackageId == packageId).ToListAsync();
                if (tempListImage.Count != listImage.Count)
                {
                    return BadRequest("Image count not match.");
                }

                var listContentTask = listImage.GroupBy(x => x.ContentTask)
                                                .Select(x => x.Key).ToList();
                var imageNote = listImage.Where(x => x.ContentTask == "0").ToList();

                foreach (var contentTask in listContentTask)
                {
                    if (contentTask == "0")
                    {
                        continue;
                    }
                    var listImageTemp = listImage.Where(x => x.ContentTask == contentTask).ToList();
                    if (imageNote.Count!=0)
                    {
                        listImageTemp.AddRange(imageNote);
                    }
                    //create task
                    var task = new Tasks()
                    {
                        PackageId = packageId,
                        TaskName = $"{package.PackageName}_Task {contentTask}",
                        Status = StatusTask.Open.ToString(),
                        StatusCheck = StatusTask.Open.ToString(),
                        StatusEntry = StatusTask.Open.ToString(),
                        StatusOp = StatusTask.Open.ToString(),
                        DateCreate = DateTime.Now,
                        DateDeadLine = DateTime.Now
                    };
                    await _context.Tasks.AddAsync(task);
                    await _context.SaveChangesAsync();

                    //đăng ký vào imagetask
                    try
                    {
                        foreach (var image in listImageTemp)
                        {
                            await _context.ImageTask.AddAsync(new ImageTask()
                            {
                                DateCreate = DateTime.Now,
                                ImageId = image.ImageId,
                                TaskId = task.TaskId
                            });
                        }

                        package.Status = StatusPackage.Open.ToString();
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception e)//fail thì xóa task mới tạo đi
                    {
                        var listTask = await _context.Tasks.Where(x => x.PackageId == packageId).ToListAsync();
                        //xóa ở bảng Imagetask
                        foreach (var taskTemp in listTask)
                        {
                            var imageTaskTemp = await _context.ImageTask
                                .Where(x => x.TaskId == taskTemp.TaskId)
                                .ToListAsync();
                            _context.ImageTask.RemoveRange(imageTaskTemp);
                        }
                        //xóa ở bảng task
                        _context.Tasks.RemoveRange(listTask);
                        await _context.SaveChangesAsync();
                        return BadRequest("Create ImageTask fail!");
                    }
                }
                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }
        /// <summary>
        /// gắn task cho image
        /// </summary>
        /// <param name="imageTask"></param>
        /// <returns></returns>
        [HttpPost("Create/{nameTask}")]
        public async Task<IActionResult> Create(List<Image> listImage, string nameTask)
        {
            try
            {
                if (!listImage.Any())
                {
                    return BadRequest("Image count 0");
                }
                var imageTemp = listImage[0];
                //create task
                var task = new Tasks()
                {
                    PackageId = imageTemp.PackageId,
                    TaskName = "Task " + nameTask,
                    Status = StatusTask.Open.ToString(),
                    StatusCheck = StatusTask.Open.ToString(),
                    StatusEntry = StatusTask.Open.ToString(),
                    StatusOp = StatusTask.Open.ToString(),
                    DateCreate = DateTime.Now,
                    DateDeadLine = DateTime.Now
                };
                await _context.Tasks.AddAsync(task);
                await _context.SaveChangesAsync();

                //đăng ký vào imagetask
                try
                {
                    foreach (var image in listImage)
                    {
                        await _context.ImageTask.AddAsync(new ImageTask()
                        {
                            DateCreate = DateTime.Now,
                            ImageId = image.ImageId,
                            TaskId = task.TaskId
                        });
                    }
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)//fail thì xóa task mới tạo đi
                {
                    _context.Tasks.Remove(task);
                    await _context.SaveChangesAsync();
                    return BadRequest("Create ImageTask fail!");
                }
                return Ok(task);
            }
            catch (Exception e)
            {
                return BadRequest(e.StackTrace);
            }
        }

    }
}