﻿authen

nuget
	  Microsoft.AspNetCore.Authentication.JwtBearer
create auhen jwt
    step1: add info in 'appsettings.json'
        "ConnectionString": {
            //"DpcEntryDB": "server=CPP00152231B\\SQLEXPRESsS;database=DpcEntry;User ID=sa;password=Hacking@12345;"
            "DpcEntryDB": "server=localhost;port=3306;database=DpcEntry;User ID=root;password=Hacking12345;"
        },
        "AppSettings":{
            "Token": "secret key for jwt"
        }
    step2: add code in 'status.cs'
        ConfigureServices
            //fix lỗi khi include entity Microsoft.AspNetCore.Mvc.NewtonsoftJson
            services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);
            var key = Encoding.ASCII.GetBytes(Configuration.GetSection("AppSettings:Token").Value);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
        Configure
            app.UseAuthentication();
    Step3: code registry token
        var tokenHandler = new JwtSecurityTokenHandler();
        var AppSettingstoken = _config["AppSettings:Token"];
        var key = Encoding.ASCII.GetBytes(AppSettingstoken);
        var tokenDescriptor = new SecurityTokenDescriptor
        {
            Subject = new ClaimsIdentity(new Claim[]{
                    new Claim(ClaimTypes.NameIdentifier,_acc.AccId.ToString()),
                    new Claim(ClaimTypes.Name, _acc.AccName),
                    new Claim(ClaimTypes.Role, _acc.Role==null?"":_acc.Role),
                }),
            Expires = DateTime.Now.AddDays(1),
            SigningCredentials = new SigningCredentials(
                            new SymmetricSecurityKey(key),
                            SecurityAlgorithms.HmacSha512Signature)
        };

        var token = tokenHandler.CreateToken(tokenDescriptor);
        var IdTokenTraining = tokenHandler.WriteToken(token);
Apicontroller
    [Route("api/[controller]")]
    [ApiController]

	//BadRequest(); 400  sai dinh dạng
	//NotFound();	404 không tìm thấy
	//Ok("value");	200 ok
	//Unauthorized()401 lỗi đăng nhập authen
	//Created()		201 đã tạo
	//Challenge()	403 ko có quyền

	private readonly ILogger<HomeController> _logger;
	private readonly DpcEntryContext _context;
    private readonly IConfiguration _config;
    public HomeController(DpcEntryContext context, IConfiguration config,ILogger<HomeController> logger)
    {
        _context = context;
        _config = config;
        _logger = logger;
    }
	public async Task<IActionResult> Create() //Read()//Update()//Delete()
    {
        try {
            return Ok();
        } catch (Exception ex)
        {
            return BadRequest(ex.ToString());
        }
    }
    [HttpGet("getValue/{naa}")]
    [Authorize]
    public async Task<IActionResult> GetValues(string naa)
    {
	    Ok("value");
    }