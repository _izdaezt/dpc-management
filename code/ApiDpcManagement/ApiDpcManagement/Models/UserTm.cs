﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ApiDpcManagement.Models
{
    public class UserTm
    {
        [JsonProperty("username")]
        public string Username { get; set; }
          [JsonProperty("password")]
        public string Password { get; set; }
          [JsonProperty("rememberMe")]
        public bool RememberMe { get; set; }
    }
    public class LoginToken
    {
        public string Login { get; set; }
         [JsonProperty("id_Token")]
        public string IdToken { get; set; }
        public string IdTokenManagement { get; set; }
        public long AccId { get; set; }
    }
}
