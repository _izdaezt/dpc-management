﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiDpcManagement
{
    public class Global
    {
    }

    enum StatusPackage
    {
        Na,
        Uploaded,
        Tasking,
        Open,
        Doing,
        Done
    }
    enum StatusTask
    {
        Na,
        Open,
        Doing,
        Done
    }
}
