﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using DalApiDpcManagement.ClassModel;
using DalApiDpcManagement.Models;
using Newtonsoft.Json;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Exporting;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Interactive;
using Syncfusion.Pdf.Parsing;
using Color = Syncfusion.Drawing.Color;
using Image = System.Drawing.Image;
using PointF = Syncfusion.Drawing.PointF;
using SizeF = Syncfusion.Drawing.SizeF;
using RectangleF = Syncfusion.Drawing.RectangleF;
namespace ServiceHelper.Service
{

    public class PdfService
    {
        public PdfService()
        {
            // Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NzM3NEAzMTM3MmUzNDJlMzBPRm41TTBEL2hiZ0pjbG93dDZPQ0VocmRCWkJHSXlzWFgrUkxrZVlDaUpzPQ==");//17.x
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MTMzN0AzMTM4MmUzMTJlMzBYTml4RFZ2ZmVsRmlNbmdCcDNjVG9naS9qWEFzVXJvL0FkSmlJbnkzVHV3PQ==");//18.x

        }
        /// <summary>
        /// Extrach pdf-> image
        /// </summary>
        /// <param name="pathFilePdf"></param>
        /// <returns></returns>
        /// <exception cref=""></exception>
        public List<string> ToImage(string pathFilePdf)
        {
            try
            {
                List<string> result = new List<string>();
                string fileName = Path.GetFileNameWithoutExtension(pathFilePdf);
                string pathExport = Path.Combine(Path.GetDirectoryName(pathFilePdf), fileName);
                if (!Directory.Exists(pathExport))
                {
                    Directory.CreateDirectory(pathExport);
                }
                Stream stream = new FileStream(pathFilePdf, FileMode.Open);
                PdfLoadedDocument loadedDocument = new PdfLoadedDocument(stream);

                Image[] images = null;

                for (int i = 0; i < loadedDocument.Pages.Count; i++)
                {
                    images = null;
                    //Get the page 
                    PdfLoadedPage page = loadedDocument.Pages[i] as PdfLoadedPage;

                    //Extract images 
                    images = page.ExtractImages();

                    for (int j = 0; j < images.Length; j++)
                    {
                        //Save the image to memory stream
                        MemoryStream imageStream = new MemoryStream();
                        images[j].Save(imageStream, ImageFormat.Png);
                        //Save the image to local file
                        imageStream.Position = 0;
                        string pathFileSave = Path.Combine(pathExport, $"{fileName}-{i + 1}-{j + 1}.png");
                        FileStream outStream = System.IO.File.OpenWrite(pathFileSave);
                        imageStream.WriteTo(outStream);
                        outStream.Flush();
                        imageStream.Dispose();
                        result.Add(pathFileSave);
                    }
                }

                //Dispose the stream 
                stream.Dispose();
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw new Exception(e.StackTrace);
            }

        }


        /// <summary>
        /// demo
        /// </summary>
        public void ToPdf()
        {
            var listImage = new List<string>()
            {
                @"D:\DPS_NAMNN14\Entry\yamato-entry\CodeGIT\edp-yamato-entry\docs\YAMATO-POC2\JsonCut\YamatoCut\20200218_1_0001.jpg",
                @"D:\DPS_NAMNN14\DPC\dpc-dtp-entry\code\ApiDpcManagement\test\bin\Debug\netcoreapp3.1\20191031計算の力だめし日文3年本文【原稿入稿】-納期1113-0-0.png"
            };
            var path = @"D:\DPS_NAMNN14\Entry\yamato-entry\CodeGIT\edp-yamato-entry\docs\YAMATO-POC2\JsonCut\YamatoCut\20200218_1_0001.jpg";

            PdfDocument document = new PdfDocument();
            document.PageSettings.Margins.All = 0;
            foreach (var imageTemp in listImage)
            {
                MemoryStream imageStream2 = new MemoryStream(File.ReadAllBytes(imageTemp));
                PdfImage image = PdfImage.FromStream(imageStream2);
                document.PageSettings.Orientation = image.Width > image.Height
                    ? PdfPageOrientation.Landscape
                    : PdfPageOrientation.Portrait;

                document.PageSettings.Size = new SizeF(image.Width, image.Height);
                //Add a page.
                PdfPage page = document.Pages.Add();

                PdfGraphics graphics = page.Graphics;

                //Create PDF graphics for the page.
                SizeF pageSize = page.GetClientSize();
                RectangleF imageBounds = new RectangleF(0, 0, image.Width, image.Height);

                graphics.DrawImage(image, imageBounds);
                //Create a solid brush.

                PdfBrush brush = new PdfSolidBrush(Color.Black);


                //Set the font.

                PdfFont font = new PdfStandardFont(PdfFontFamily.TimesRoman, 14);

                //Draw the text.

                graphics.DrawString("gắn thẻ!", font, brush, new PointF(20, 20));
                //
                //Creates a rectangle

                RectangleF rectangle = new RectangleF(10, 40, 300, 30);

                //Creates a new popup annotation.

                //PdfLoadedRectangleAnnotation 
                PdfPopupAnnotation popupAnnotation = new PdfPopupAnnotation(rectangle, "Test popup annotation");

                popupAnnotation.Border.Width = 4;

                popupAnnotation.Border.HorizontalRadius = 20;

                popupAnnotation.Border.VerticalRadius = 30;

                //Sets the pdf popup icon.

                popupAnnotation.Icon = PdfPopupIcon.Insert;

                //Adds this annotation to the created page.

                page.Annotations.Add(popupAnnotation);

                page.Annotations.Add(GetPdfRectangleAnnotation(new RectangleF(10, 40, 300, 30), "nguyễn ngọc nam", Color.Red, "123", "321", 2));

            }

            //Save the document.

            MemoryStream imageStream = new MemoryStream();
            document.Save(imageStream);
            SaveFile(imageStream, "output.pdf");

            //document.Save(new stream "Output.pdf");
            document.Close(true);
        }
        /// <summary>
        /// tạo tag để gắn vào pdf
        /// </summary>
        /// <param name="rectangleF"></param>
        /// <param name="strComment"></param>
        /// <param name="color"></param>
        /// <param name="subject"></param>
        /// <param name="author"></param>
        /// <param name="border"></param>
        /// <returns></returns>
        public PdfRectangleAnnotation GetPdfRectangleAnnotation(RectangleF rectangleF, string strComment, Color color
            , string subject = "", string author = "", int border = 1)
        {
            PdfRectangleAnnotation rectangleAnnotation = new PdfRectangleAnnotation(rectangleF, strComment);
            //Set author
            rectangleAnnotation.Author = author;
            rectangleAnnotation.Border.BorderWidth = border;
            rectangleAnnotation.Color = color;
            rectangleAnnotation.Subject = subject;
            rectangleAnnotation.ModifiedDate = DateTime.Now;
            return rectangleAnnotation;

        }

        public void SaveFile(MemoryStream stream, string pathFileSave)
        {
            //Save the image to local file
            stream.Position = 0;
            FileStream outStream = System.IO.File.OpenWrite(pathFileSave);
            stream.WriteTo(outStream);
            outStream.Flush();
            stream.Dispose();
        }

        public void Read()
        {
            //Load the PDF document 
            string path = @"D:\Deploy\Upload\20191031\20191031計算の力だめし日文3年本文【原稿入稿】-納期1113.pdf";//_hostingEnvironment.WebRootPath + "/Data/Essential_Pdf.pdf";
                                                                                              // string path = @"D:\Deploy\Upload\買掛自動照合1231-10.pdf";
            Stream stream = new FileStream(path, FileMode.Open);
            PdfLoadedDocument loadedDocument = new PdfLoadedDocument(stream);

            Image[] images = null;

            for (int i = 0; i < loadedDocument.Pages.Count; i++)
            {
                images = null;

                //Get the page 
                PdfLoadedPage page = loadedDocument.Pages[i] as PdfLoadedPage;

                //Extract images 
                images = page.ExtractImages();

                for (int j = 0; j < images.Length; j++)
                {
                    //Save the image to memory stream
                    MemoryStream imageStream = new MemoryStream();
                    images[j].Save(imageStream, ImageFormat.Png);

                    //Save the image to local file
                    imageStream.Position = 0;
                    FileStream outStream = System.IO.File.OpenWrite("Test" + $"{i}-{j}.png");
                    imageStream.WriteTo(outStream);
                    outStream.Flush();
                    imageStream.Dispose();
                }
            }

            //Dispose the stream 
            stream.Dispose();
        }

        #region Export PDF


        /// <summary>
        /// hàm này xuất pdf gồm 1 trang ảnh kèm 1 trang text các chi thị, trang đầu là vẽ
        /// rectangle và đánh số, trang 2 là note chi thị
        /// </summary>
        public MemoryStream ImageToPdfNote(Tasks task, string pathLocalFolder)
        {
            PdfBrush brush = new PdfSolidBrush(Color.Black);
            PdfPen pen = new PdfPen(Color.Black, 2);
            PdfFont font = new PdfCjkStandardFont(PdfCjkFontFamily.HeiseiMinchoW3, 14);// PdfStandardFont(PdfFontFamily.TimesRoman, 14);
                                                                                       //PdfFont font2 = new PdfTrueTypeFont(new Font("Arial Unicode MS", 14), true);
            PdfDocument document = new PdfDocument();
            document.PageSettings.Margins.All = 0;
            foreach (var imageTask in task.ImageTask)
            {
                var listTagging = JsonConvert.DeserializeObject<List<TaggingModel>>(imageTask.Image.ImageTagEntry.Value);
                var pathFileImage = Path.Combine(pathLocalFolder, imageTask.Image.ImagePath);
                MemoryStream imageStream = new MemoryStream(File.ReadAllBytes(pathFileImage));
                PdfImage image = PdfImage.FromStream(imageStream);
                document.PageSettings.Orientation = image.Width > image.Height
                    ? PdfPageOrientation.Landscape
                    : PdfPageOrientation.Portrait;
                document.PageSettings.Size = new SizeF(image.Width, image.Height);

                //Add a page.

                #region Add ảnh và vẽ các rectangle lên ảnh vào page pdf
                PdfPage page = document.Pages.Add();
                PdfGraphics graphics = page.Graphics;
                RectangleF imageBounds = new RectangleF(0, 0, image.Width, image.Height);
                graphics.DrawImage(image, imageBounds);
                foreach (var tagging in listTagging)
                {
                    var rect = CreateRectangleF(tagging.X, tagging.Y, tagging.W, tagging.H, image.Width, image.Height);
                    graphics.DrawRectangle(pen,rect);
                    graphics.DrawString(tagging.Index.ToString(), font, brush, new PointF(rect.X, rect.Y));
                }

                #endregion

                #region Add page chứa các note của ảnh trên
                PdfPage pageNote = document.Pages.Add();
                PdfGraphics graphicsNote = pageNote.Graphics;
                string note = "";
                foreach (var tagging in listTagging)
                {
                    note += $"Index:{tagging.Index}   Note:{tagging.Note} " + Environment.NewLine;
                }
                graphicsNote.DrawString(note, font, brush, new PointF(50, 50));
                #endregion
            }
            MemoryStream pdfStream = new MemoryStream();
            document.Save(pdfStream);
            document.Close(true);
            pdfStream.Position = 0;
            return pdfStream;
        }

        /// <summary>
        /// hàm này xuất pdf tag nằm ngay trên pdf 
        /// </summary>
        public MemoryStream ImageToPdfTag(Tasks task, string pathLocalFolder)
        {
            PdfBrush brush = new PdfSolidBrush(Color.Black);
            PdfPen pen = new PdfPen(Color.Black, 2);
            PdfFont font = new PdfStandardFont(PdfFontFamily.TimesRoman, 14);
            PdfDocument document = new PdfDocument();
            document.PageSettings.Margins.All = 0;
            foreach (var imageTask in task.ImageTask)
            {
                var listTagging = JsonConvert.DeserializeObject<List<TaggingModel>>(imageTask.Image.ImageTagEntry.Value);
                var pathFileImage = Path.Combine(pathLocalFolder, imageTask.Image.ImagePath);
                MemoryStream imageStream = new MemoryStream(File.ReadAllBytes(pathFileImage));
                PdfImage image = PdfImage.FromStream(imageStream);
                document.PageSettings.Orientation = image.Width > image.Height
                    ? PdfPageOrientation.Landscape
                    : PdfPageOrientation.Portrait;
                document.PageSettings.Size = new SizeF(image.Width, image.Height);

                //Add a page.

                #region Add ảnh và vẽ các tag lên ảnh vào page pdf
                PdfPage page = document.Pages.Add();
                PdfGraphics graphics = page.Graphics;
                RectangleF imageBounds = new RectangleF(0, 0, image.Width, image.Height);
                graphics.DrawImage(image, imageBounds);
                foreach (var tagging in listTagging)
                {
                    page.Annotations.Add(
                        GetPdfRectangleAnnotation(
                            CreateRectangleF(tagging.X, tagging.Y, tagging.W, tagging.H, image.Width, image.Height)
                            , tagging.Note, Color.Red, "", "", 2));
                }
                #endregion
            }
            MemoryStream pdfStream = new MemoryStream();
            document.Save(pdfStream);
            document.Close(true);
            pdfStream.Position = 0;
            return pdfStream;
        }
        /// <summary>
        /// khởi tạo rectangle bt
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        public RectangleF CreateRectangleF(float x, float y, float w, float h,int widthImage,int heightImage)
        {
            return new RectangleF(x,y,w,h);
            //return CreateRectangleFScale(x, y, w, h,widthImage,heightImage);
        }
        /// <summary>
        /// khởi tạo rectangle khi ảnh bị scale
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="w"></param>
        /// <param name="h"></param>
        /// <returns></returns>
        public RectangleF CreateRectangleFScale(float x, float y, float w, float h,int width, int height)
        {
            var widthScale = 800.0;
            var heightScale = 600.0;
            var scaleW =(float)(width / widthScale);
            var scaleH = (float)(height / heightScale);
            return new RectangleF(x*scaleW,y*scaleH,w*scaleW,h*scaleH);
        }
        #endregion
    }
}
