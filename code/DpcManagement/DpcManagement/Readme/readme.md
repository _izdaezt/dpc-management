﻿private readonly ILogger<HomeController> _logger;
private readonly DpcEntryContext _context;
private readonly IConfiguration _config;
public HomeController(DpcEntryContext context, IConfiguration config,ILogger<HomeController> logger)
{
    _context = context;
    _config = config;
    _logger = logger;
}
