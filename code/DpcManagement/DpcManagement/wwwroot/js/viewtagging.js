﻿
var rowSelectedIndex = 0;
const options = {
    drawRect: false,
    onlyOne: false,
    rectProps: {
        stroke: 'red',
        strokeWidth: 1,
        fill: ''
    }
}
var scale = 1;
var canvasScale = 1;
var SCALE_FACTOR = 1.10;

var zoomNumber = 0;
var urlImage = '';

var elmnt = document.getElementById("canvasBox");
var divHeight = elmnt.offsetHeight;
var divWidth = elmnt.offsetWidth;
const canvas = document.querySelector('#canvas')
var fabricCanvas = new fabric.Canvas(canvas);

function RowSelected(index) {
    var Image = listImage[index].image;
    var pathImage = baseURLData +
        Image.imagePath;


    var objects = fabricCanvas.getObjects();

    for (var i = 0; i < objects.length; i++) {
        fabricCanvas.remove(objects[i]);
    }

    setCanvasBackgroundImageUrl(pathImage, Image.imageTagEntry.value );
    $('#ImageName').html(Image.imageName);
    /*if (Image.imageTagEntry.value) {
        Render(Image.imageTagEntry.value);
    }*/

}


function ShowContentOfRec() {
    var selection = fabricCanvas.getActiveObject();
    if (selection) {
        if (selection.type === 'activeSelection') {
            /*
              selection.forEachObject(function(element) {
                  console.log(element);
                  fabricCanvas.remove(element);
              });*/
        }
        else {
            $('#note').val(selection.note);
        }
    }
}


function Next() {

    if (rowSelectedIndex == 0) {
        $('#backward').removeClass("disabled");
    }
    rowSelectedIndex++;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex >= listImage.length - 1) {
        $('#forward').addClass("disabled");
    }
}

function Prev() {

    if (rowSelectedIndex == listImage.length - 1) {
        $('#forward').removeClass("disabled");
    }
    rowSelectedIndex--;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex == 0) {
        $('#backward').addClass("disabled");
    }
}

function Render(arrRec) {
    if (arrRec) {
        arrRec = JSON.parse(arrRec);
        arrRec.forEach(function (item) {


            rect = new fabric.Rect({
                left: item.left / scale,
                top: item.top / scale,
                width: item.width / scale, height: item.height / scale,
                ...options.rectProps,
                note: item.note,
                Index: item.index,
                IsJapan: item.IsJapan,
            });
            rect.lockMovementX = true;
            rect.lockMovementY = true;
            rect.lockScalingX = true;
            rect.lockScalingY = true;
            rect.hasRotatingPoint = false;
            fabricCanvas.add(rect)
            rect.on('mousedown', function (e) {

                console.log('mousedown');
                console.log(e);
                ShowContentOfRec();
            });
        });

        fabricCanvas.renderAll();
    }
}


function setCanvasBackgroundImageUrl(url ,arr) {
    if (url && url.length > 0) {
        fabric.Image.fromURL(url, function (img) {
            bgImage = img;
            urlImage = url;

            scale = img.width / divWidth; 
            fabricCanvas.setWidth(divWidth);
            fabricCanvas.setHeight(img.height / scale);

            fabricCanvas.setBackgroundImage(img, fabricCanvas.renderAll.bind(fabricCanvas), {
                scaleX: fabricCanvas.width / img.width,
                scaleY: fabricCanvas.height / img.height
            });
            Render(arr)
        });
    }
}

function setCanvasBackgroundImageUrlAndScale(url) {
    if (url && url.length > 0) {
        fabric.Image.fromURL(url, function (img) {
            bgImage = img;
            fabricCanvas.setBackgroundImage(img, fabricCanvas.renderAll.bind(fabricCanvas), {
                scaleX: fabricCanvas.width / img.width,
                scaleY: fabricCanvas.height / img.height
            });
        });
    } else {
        fabricCanvas.backgroundImage = 0;
        fabricCanvas.setBackgroundImage('', fabricCanvas.renderAll.bind(fabricCanvas));

        fabricCanvas.renderAll();
    }
}


jQuery(window).load(function () {
    setTimeout(function () {

        RowSelected(rowSelectedIndex);
        $("#btnSubmit").attr("disabled", true);
        /*
        $("#note").on("keyup", function (e) {

            var selection = fabricCanvas.getActiveObject();
            if (selection) {
                if (selection.type === 'activeSelection') {

                }
                else {
                    selection.note = this.value;
                }
            }
        });*/
    }, 600);
});


$('#canvasBox').bind('mousewheel', function (e) {
    if (e.altKey == true) {
        e.preventDefault();
        if (e.originalEvent.wheelDelta / 120 > 0) {
            zoomIn();
            zoomNumber++;
        } else {
            zoomOut();
            zoomNumber--;
        }
    }
});


$('#zoomIn').on('click', function (event) {
    zoomIn();
    zoomNumber++;
});

$('#zoomOut').on('click', function (event) {
    zoomOut();
    zoomNumber--;
});

$('#resetZoom').on('click', function (event) {
    zoomDefault();
    zoomNumber = 0;
});
function zoomIn() {

    canvasScale = canvasScale * SCALE_FACTOR;

    fabricCanvas.setHeight(fabricCanvas.getHeight() * SCALE_FACTOR);
    fabricCanvas.setWidth(fabricCanvas.getWidth() * SCALE_FACTOR);

    setCanvasBackgroundImageUrlAndScale(urlImage);

    var objects = fabricCanvas.getObjects();
    for (var i in objects) {
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        var tempScaleX = scaleX * SCALE_FACTOR;
        var tempScaleY = scaleY * SCALE_FACTOR;
        var tempLeft = left * SCALE_FACTOR;
        var tempTop = top * SCALE_FACTOR;

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft;
        objects[i].top = tempTop;

        objects[i].setCoords();
    }


    fabricCanvas.renderAll();
}

function zoomOut() {

    canvasScale = canvasScale / SCALE_FACTOR;

    fabricCanvas.setHeight(fabricCanvas.getHeight() * (1 / SCALE_FACTOR));
    fabricCanvas.setWidth(fabricCanvas.getWidth() * (1 / SCALE_FACTOR));

    setCanvasBackgroundImageUrlAndScale(urlImage);

    var objects = fabricCanvas.getObjects();
    for (var i in objects) {
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        var tempScaleX = scaleX * (1 / SCALE_FACTOR);
        var tempScaleY = scaleY * (1 / SCALE_FACTOR);
        var tempLeft = left * (1 / SCALE_FACTOR);
        var tempTop = top * (1 / SCALE_FACTOR);

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft;
        objects[i].top = tempTop;

        objects[i].setCoords();
    }

    fabricCanvas.renderAll();
}

function zoomDefault() {
    if (zoomNumber > 0) {
        for (var i = 0; i < zoomNumber; i++) {
            zoomOut();
        }
    }
    if (zoomNumber < 0) {
        for (var i = 0; i < Math.abs(zoomNumber); i++) {
            zoomIn();
        }
    }

}