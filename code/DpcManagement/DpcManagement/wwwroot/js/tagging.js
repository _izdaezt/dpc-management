﻿var canvasImage = [];
var timeStart = Date.now();
var rowSelectedIndex = 0;

const canvas = document.querySelector('#canvas')
var fabricCanvas = new fabric.Canvas(canvas);

var elmnt = document.getElementById("canvasBox");
var canvasScale = 1;
var canvasWidth = 800;
var canvasHeight = 600;
var canvasOriginalWidth = 800;
var canvasOriginalHeight = 600;
var bgImage;
var divHeight = elmnt.offsetHeight;
var divWidth = elmnt.offsetWidth;

var scaleFirst = 1;
var SCALE_FACTOR = 1.10;
var urlImage = "";

var zoomNumber = 0;

fabricCanvas.setWidth(canvasWidth);
fabricCanvas.setHeight(canvasHeight);

function setCanvasBackgroundImageUrl(url) {
    if (url && url.length > 0) {
        fabric.Image.fromURL(url, function (img) {
            bgImage = img;
            urlImage = url;
            scaleFirst = img.width / divWidth;

            fabricCanvas.setWidth(divWidth);
            fabricCanvas.setHeight(img.height / scaleFirst);

            fabricCanvas.setBackgroundImage(img, fabricCanvas.renderAll.bind(fabricCanvas), {
                scaleX: fabricCanvas.width / img.width,
                scaleY: fabricCanvas.height / img.height
            });
            //scaleAndPositionImage();
        });
    } else {
        fabricCanvas.backgroundImage = 0;
        fabricCanvas.setBackgroundImage('', fabricCanvas.renderAll.bind(fabricCanvas));

        fabricCanvas.renderAll();
    }
}

function setCanvasBackgroundImageUrlAndScale(url) {
    if (url && url.length > 0) {
        fabric.Image.fromURL(url, function (img) {
            bgImage = img;
            fabricCanvas.setBackgroundImage(img, fabricCanvas.renderAll.bind(fabricCanvas), {
                scaleX: fabricCanvas.width / img.width,
                scaleY: fabricCanvas.height / img.height
            });
        });
    } else {
        fabricCanvas.backgroundImage = 0;
        fabricCanvas.setBackgroundImage('', fabricCanvas.renderAll.bind(fabricCanvas));

        fabricCanvas.renderAll();
    }
}


//setCanvasBackgroundImageUrl(imageUrl);
// Define 
const checkbox = document.querySelector('#checkbox')
let initialPos, bounds, rect, dragging = false, freeDrawing = checkbox.checked
const options = {
    drawRect: drawRect.checked,
    onlyOne: onlyOne.checked,
    rectProps: {
        stroke: 'red',
        strokeWidth: 1,
        fill: ''
    }
}
function onMouseDown(e) {
    dragging = true;
    if (!freeDrawing) {
        return
    }
    initialPos = { ...e.pointer }
    bounds = {}
    if (options.drawRect) {
        rect = new fabric.Rect({
            left: initialPos.x,
            top: initialPos.y,
            width: 0, height: 0,
            ...options.rectProps,
            note: "",
            Index: getIndex(),
            IsJapan: true,
        });
        /*  rect.toObject = (function(toObject) {
    return function(propertiesToInclude) {
      return fabric.util.object.extend(toObject.call(this, propertiesToInclude), {
        contents:'abc'
      });
    };
  })(rect.toObject);*/
        fabricCanvas.add(rect)
        rect.on('mousedown', function (e) {

            console.log('mousedown');
            console.log(e);
            ShowContentOfRec();
        });
    }
    ShowContentOfRec();
}
function update(pointer) {
    if (initialPos.x > pointer.x) {
        bounds.x = Math.max(0, pointer.x)
        bounds.width = initialPos.x - bounds.x;
    } else {
        bounds.x = initialPos.x
        bounds.width = pointer.x - initialPos.x
    }
    if (initialPos.y > pointer.y) {
        bounds.y = Math.max(0, pointer.y)
        bounds.height = initialPos.y - bounds.y
    } else {
        bounds.height = pointer.y - initialPos.y
        bounds.y = initialPos.y
    }
    if (options.drawRect) {
        rect.left = bounds.x
        rect.top = bounds.y
        rect.width = bounds.width
        rect.height = bounds.height
        rect.dirty = true
        fabricCanvas.requestRenderAllBound()
    }
}
function onMouseMove(e) {
    if (!dragging || !freeDrawing) {
        return
    }
    requestAnimationFrame(() => update(e.pointer))
}
function onMouseUp(e) {
    dragging = false;
    if (!freeDrawing) { return }
    if (options.drawRect && rect && (rect.width == 0 || rect.height === 0)) {
        fabricCanvas.remove(rect)
    }

    if (!options.drawRect || !rect) {
        rect = new fabric.Rect({
            ...bounds, left: bounds.x, top: bounds.y,
            ...options.rectProps,
            note: "",
            Index: getIndex(),
            IsJapan: true,
        });

        /* rect.toObject = (function(toObject) {
   return function(propertiesToInclude) {
     return fabric.util.object.extend(toObject.call(this, propertiesToInclude), {
       contents:'abc'
     });
   };
 })(rect.toObject);*/

        fabricCanvas.add(rect)
        rect.on('mousedown', function (e) {

            console.log('mousedown');
            console.log(e);
            ShowContentOfRec();
        });
        rect.dirty = true
        fabricCanvas.requestRenderAllBound()
    }
    rect.setCoords() // important! 
    options.onlyOne && uninstall()
    //getCoordinates()
    getCoordinatesOnDraw();
    ShowContentOfRec();
}
function install() {
    freeDrawing = true; dragging = false; rect = null
    checkbox.checked = true
    fabricCanvas.on('mouse:down', onMouseDown);
    fabricCanvas.on('mouse:move', onMouseMove);
    fabricCanvas.on('mouse:up', onMouseUp);
}
function uninstall() {
    freeDrawing = false; dragging = false; rect = null
    checkbox.checked = false
    fabricCanvas.off('mouse:down', onMouseDown);
    fabricCanvas.off('mouse:move', onMouseMove);
    fabricCanvas.off('mouse:up', onMouseUp);
}

// the following is OOT - it's just for the controls above
checkbox.addEventListener('change', e =>
    e.currentTarget.checked ? install() : uninstall()
)
document.querySelector('#drawRect').addEventListener('change', e => {
    options.drawRect = e.currentTarget.checked
})
document.querySelector('#onlyOne').addEventListener('change', e => {
    options.onlyOne = e.currentTarget.checked
})
freeDrawing && install()
/*
document.querySelector('#changeCanvasPosition').addEventListener('click', () => {
  const el = document.querySelector(`.wrapper`)
  el.style.marginTop = Math.trunc(Math.random() * 300) + 'px'
  el.style.marginLeft = Math.trunc(Math.random() * 200) + 'px'
})
*/
$('html').keyup(function (e) {
    if (e.keyCode == 46) {
        deleteSelectedObjectsFromCanvas();
    }
   
    if (e.keyCode == 17) {
        if (checkbox.checked === false)
            install();
        else
            uninstall();
    }
});
function deleteSelectedObjectsFromCanvas() {
    var selection = fabricCanvas.getActiveObject();
    if (selection.type === 'activeSelection') {
        selection.forEachObject(function (element) {
            console.log(element);
            fabricCanvas.remove(element);
        });
    }
    else {
        fabricCanvas.remove(selection);
    }
    fabricCanvas.discardActiveObject();
    fabricCanvas.renderAll();
}

function getCoordinates() {
    var coords = [];
    fabricCanvas.forEachObject(function (obj) {
        var prop = {
            left: obj.left,
            top: obj.top,
            width: obj.width * obj.zoomX,
            height: obj.height * obj.zoomY,
            note: obj.note,
            Index: obj.Index,
            IsJapan: obj.IsJapan,
        };
        coords.push(prop);
    });

    if (coords.length > 0) {
        var obj = {};
        var ImageId = listImage[rowSelectedIndex].image.imageId;
        obj["id"] = ImageId;
        obj["value"] = coords;
        obj["scale"] = scaleFirst;

        for (let i = 0; i < canvasImage.length; i++) {
            if (canvasImage[i].id == ImageId) {
                canvasImage.splice(i, 1);
            }
        }
        canvasImage.push(obj)
        console.log(obj)
    }
    CheckEnableSubmit();
}

function getCoordinatesOnDraw() {
    var coords = [];
    fabricCanvas.forEachObject(function (obj) {
        var prop = {
            left: obj.left,
            top: obj.top,
            width: obj.width,
            height: obj.height,
            note: obj.note,
            Index: obj.Index,
            IsJapan: obj.IsJapan,
        };
        coords.push(prop);
    });

    if (coords.length > 0) {
        var obj = {};
        var ImageId = listImage[rowSelectedIndex].image.imageId;
        obj["id"] = ImageId;
        obj["value"] = coords;
        obj["scale"] = scaleFirst;

        for (let i = 0; i < canvasImage.length; i++) {
            if (canvasImage[i].id == ImageId) {
                canvasImage.splice(i, 1);
            }
        }
        canvasImage.push(obj)
        console.log(obj)
    }
    CheckEnableSubmit();
}


function CheckEnableSubmit(parameters) {
    if (canvasImage.length == listImage.length) {
        $("#btnSubmit").attr("disabled", false);

    } else {
        $("#btnSubmit").attr("disabled", true);
    }
}
function ShowContentOfRec() {
    var selection = fabricCanvas.getActiveObject();
    if (selection) {
        if (selection.type === 'activeSelection') {
            /*
              selection.forEachObject(function(element) {
                  console.log(element);
                  fabricCanvas.remove(element);
              });*/
        }
        else {
            $('#note').val(selection.note);
        }
    }
}
jQuery(window).load(function () {
    setTimeout(function () {
        RowSelected(rowSelectedIndex);
        $("#btnSubmit").attr("disabled", true);

        $("#note").on("keyup", function (e) {

            var selection = fabricCanvas.getActiveObject();
            if (selection) {
                if (selection.type === 'activeSelection') {
                    /*
                      selection.forEachObject(function(element) {
                          console.log(element);
                          fabricCanvas.remove(element);
                      });*/
                }
                else {
                    selection.note = this.value;
                }
            }
        });
    }, 600);
});

function RowSelected(index) {
    var Image = listImage[index].image;
    var pathImage = baseURLData +
        Image.imagePath;

    var objects = fabricCanvas.getObjects();

    for (var i = 0; i < objects.length; i++) {
        fabricCanvas.remove(objects[i]);
    }

    zoomNumber = 0;
    setCanvasBackgroundImageUrl(pathImage)

    //fabricCanvas = new fabric.Canvas(canvas);
    $('#ImageName').html(Image.imageName);

    var imageId = Image.imageId;

    for (let i = 0; i < canvasImage.length; i++) {
        if (canvasImage[i].id == imageId) {
            Render(canvasImage[i].value);
        }
    }
}

function Next() {

    if (rowSelectedIndex == 0) {
        $('#backward').removeClass("disabled");
    }

    zoomDefault();
    getCoordinates();
    rowSelectedIndex++;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex >= listImage.length - 1) {
        $('#forward').addClass("disabled");
    }
}

function Prev() {

    if (rowSelectedIndex == listImage.length - 1) {
        $('#forward').removeClass("disabled");
    }
    zoomDefault();
    getCoordinates();
    rowSelectedIndex--;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex == 0) {
        $('#backward').addClass("disabled");
    }
}


function Submit() {
    getCoordinates();
    var timeEnd = Date.now();

    var a = moment(timeStart);
    var b = moment(timeEnd);
    var secons = b.diff(a, 's');

    listImage.forEach(function (item) {
        canvasImage.forEach(function (item1) {
            if (item.image.imageId == item1.id) {

                item1.value.forEach(function (rec) {
                    rec.left = rec.left * item1.scale;
                    rec.top = rec.top * item1.scale;
                    rec.width = rec.width * item1.scale;
                    rec.height = rec.height * item1.scale;
                });

                item.image.value = JSON.stringify(item1.value);
                item.image.duration = secons;
            }
        });
    });

    var data = { listImageTag: listImage, taskId: listTask.taskId };
    $.ajax({
        url: "/DpcManagement/PackageMdgc/SubmitTag",
        type: "post",
        data: data,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    }).done(function (data) {
        window.location.replace(data.newUrl);
    });
}

function Render(arrRec) {

    arrRec.forEach(function (item) {
        rect = new fabric.Rect({
            left: item.left,
            top: item.top,
            width: item.width, height: item.height,
            ...options.rectProps,
            note: item.note,
            Index: item.Index,
            IsJapan: item.IsJapan,
        });

        fabricCanvas.add(rect)
        rect.on('mousedown', function (e) {

            console.log('mousedown');
            console.log(e);
            ShowContentOfRec();
        });
    });

    fabricCanvas.renderAll();
    //ShowContentOfRec();
}

function getIndex() {
    var ImageId = listImage[rowSelectedIndex].image.imageId;

    for (var i = 0; i < canvasImage.length; i++) {
        var item = canvasImage[i];
        if (item.id == ImageId) {
            var lastItem = item.value[item.value.length - 1];
            return lastItem.Index + 1;
        }
    }
    return 0;
}
    
    
var TableDatatablesScroller = function () {
    var e = function () {
        var e = $("#TableImage");
        e.dataTable({
            ordering: false,
            searching: false,
            language: {
                emptyTable: "No data available in table", info: "Showing _START_ to _END_ of _TOTAL_ entries", infoEmpty: "No entries found", infoFiltered: "(filtered1 from _MAX_ total entries)", lengthMenu: "_MENU_ entries", zeroRecords: "No matching records found"
            }
            , buttons: [], scrollY: 300, deferRender: !0, scroller: !0, deferRender: !0, scrollX: !0, scrollCollapse: !0, pageLength: 10, dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
        });
    }
    return {
        init: function () {
            jQuery().dataTable && (e())
        }
    }
}();

jQuery(document).ready(function () {
    TableDatatablesScroller.init();
});

$('#canvasBox').bind('mousewheel', function (e) {
    if (e.altKey == true) {
        e.preventDefault();
        if (e.originalEvent.wheelDelta / 120 > 0) {
            zoomIn();
            zoomNumber++;
        } else {
            zoomOut();
            zoomNumber--;
        }
    }
});


$('#zoomIn').on('click', function (event) {
    zoomIn();
    zoomNumber++;
});

$('#zoomOut').on('click', function (event) {
    zoomOut();
    zoomNumber--;
});

$('#resetZoom').on('click', function (event) {
    zoomDefault();
    zoomNumber = 0;
});
function zoomIn() {

    canvasScale = canvasScale * SCALE_FACTOR;

    fabricCanvas.setHeight(fabricCanvas.getHeight() * SCALE_FACTOR);
    fabricCanvas.setWidth(fabricCanvas.getWidth() * SCALE_FACTOR);

    setCanvasBackgroundImageUrlAndScale(urlImage);

    var objects = fabricCanvas.getObjects();
    for (var i in objects) {
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        var tempScaleX = scaleX * SCALE_FACTOR;
        var tempScaleY = scaleY * SCALE_FACTOR;
        var tempLeft = left * SCALE_FACTOR;
        var tempTop = top * SCALE_FACTOR;

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft;
        objects[i].top = tempTop;

        objects[i].setCoords();
    }


    fabricCanvas.renderAll();
}

function zoomOut() {

    canvasScale = canvasScale / SCALE_FACTOR;

    fabricCanvas.setHeight(fabricCanvas.getHeight() * (1 / SCALE_FACTOR));
    fabricCanvas.setWidth(fabricCanvas.getWidth() * (1 / SCALE_FACTOR));

    setCanvasBackgroundImageUrlAndScale(urlImage);
    
    var objects = fabricCanvas.getObjects();
    for (var i in objects) {
        var scaleX = objects[i].scaleX;
        var scaleY = objects[i].scaleY;
        var left = objects[i].left;
        var top = objects[i].top;

        var tempScaleX = scaleX * (1 / SCALE_FACTOR);
        var tempScaleY = scaleY * (1 / SCALE_FACTOR);
        var tempLeft = left * (1 / SCALE_FACTOR);
        var tempTop = top * (1 / SCALE_FACTOR);

        objects[i].scaleX = tempScaleX;
        objects[i].scaleY = tempScaleY;
        objects[i].left = tempLeft;
        objects[i].top = tempTop;

        objects[i].setCoords();
    }

    fabricCanvas.renderAll();
}

function zoomDefault() {
    if (zoomNumber > 0) {
        for (var i = 0; i < zoomNumber; i++) {
            zoomOut();
        }
    }
    if (zoomNumber < 0) {
        for (var i = 0; i < Math.abs(zoomNumber) ; i++) {
            zoomIn();
        }
    }
    
}