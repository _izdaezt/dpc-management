﻿
var rowSelectedIndex = 0;

var arr = getUrlVars();
var PackageId = arr.PackageId;

$(document).ready(function () {
    $("#btnSubmit").attr("disabled", true);

    console.log(listImage);
    var content = '';

    $.each(listImage, function (i, item) {
        content += '<tr><td><input  id=' + item.imageId + ' type="number" class="form-control" style="width: 70px;" min="0" /></td><td>' + item.imageId + '</td><td>' + item.imageName + '</td><td>' + item.imagePath + '</td></tr>';
    });
    $('#table-list-image tbody').html(content);
    RowSelected(rowSelectedIndex);
    TableDatatablesScroller.init();

    $("#TableImage > tbody > tr").click(function () {
        console.log(111);
    });
});

function RowSelected(index) {
    var Image = listImage[index];
    var pathImage = Image.imagePath;
    //document.getElementById("image").src = baseURLData + pathImage;
    $('#divImage > div').remove();
    $('#divImage').prepend('<img id="image" src="' + baseURLData + pathImage + '" width="325" height="425" style="display: none"/>');
    initImage();
    $("#" + Image.imageId + "").focus();
    $('#ImageName').html(Image.imageName);

}

function Next() {

    if (rowSelectedIndex == 0) {
        $('#backward').removeClass("disabled");
    }
    rowSelectedIndex++;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex >= listImage.length - 1) {
        $('#forward').addClass("disabled");
    }
}

function Prev() {

    if (rowSelectedIndex == listImage.length - 1) {
        $('#forward').removeClass("disabled");
    }
    rowSelectedIndex--;
    RowSelected(rowSelectedIndex);
    if (rowSelectedIndex == 0) {
        $('#backward').addClass("disabled");
    }
}


function Submit() {

    var inputValues = new Array();
    $('input').each(function () {
        if ($(this).attr('id')) {
            var obj = {};
            obj["id"] = $(this).attr('id');
            obj["value"] = $(this).val();
            inputValues.push(obj);
        }
    });

    var arrImage = [];
    for (var i = 0; i < inputValues.length; i++) {
        var found = listImage.find(x => x.imageId == inputValues[i].id);
        found.contentTask = inputValues[i].value;
        arrImage.push(found);
    }
    var datas = { listImage: arrImage };
    $.ajax({
        url: "ImageTask/Index",
        type: "post",
        data: datas,
        success: function (data) {
            console.log(data);
        },
        error: function (error) {
            console.log(error);
        }
    }).done(function (data) {
        window.location.replace( baseURLManagement + "Package");
    });

}

jQuery(window).load(function () {
    setTimeout(function () {
        $("#TableImage > tbody > tr").click(function () {
            var rowSelectedIndex = $(this)[0].rowIndex - 1;
            RowSelected(rowSelectedIndex);

            $('#backward').removeClass("disabled");
            $('#forward').removeClass("disabled");

            if (rowSelectedIndex == listImage.length - 1) {
                $('#forward').addClass("disabled");
            }

            if (rowSelectedIndex == 0) {
                $('#backward').addClass("disabled");
            }

        });
        $('input').keyup(function () {
            var count = 0;
            var countImage = 0;
            $('input').each(function () {
                if ($(this).attr('id')) {
                    countImage++;
                    if ($(this).val()) {
                        count++;
                    }
                }
            });
            if (countImage == count) {
                $("#btnSubmit").attr("disabled", false);
            }
            else {
                $("#btnSubmit").attr("disabled", true);
            }
        });
    }, 600);
});


var TableDatatablesScroller = function () {
    var e = function () {
        var e = $("#TableImage");
        e.dataTable({
            ordering: false,
            searching: false,
            language: {
                emptyTable: "No data available in table", info: "Showing _START_ to _END_ of _TOTAL_ entries", infoEmpty: "No entries found", infoFiltered: "(filtered1 from _MAX_ total entries)", lengthMenu: "_MENU_ entries", zeroRecords: "No matching records found"
            }
            , buttons: [], scrollY: 400, deferRender: !0, scroller: !0, deferRender: !0, scrollX: !0, scrollCollapse: !0, pageLength: 10, dom: "<'row' <'col-md-12'B>><'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>"
        });
    }
    return {
        init: function () {
            jQuery().dataTable && (e())
        }
    }
}();

function initImage() {
    $('#image').apImageZoom({
        cssWrapperClass: 'custom-wrapper-class',
        autoCenter: false,
        minZoom: 'contain',
        maxZoom: false,
        maxZoom: 1.0,
        imageUrl: undefined,
        loadingAnimation: undefined,
        loadingAnimationData: undefined,
        loadingAnimationFadeOutDuration: 200,
        cssWrapperClass: undefined,
        initialZoom: 'auto',
        minZoom: 0.2,
        maxZoom: 1.0,
        zoomStep: 0.07,
        autoCenter: true,
        hammerPluginEnabled: true,
        mouseWheelPluginEnabled: true,
        hardwareAcceleration: true,
        disabled: false,
        dragEnabled: true,
        zoomEnabled: true,
        doubleClick: 'zoomToggle'
    });
};