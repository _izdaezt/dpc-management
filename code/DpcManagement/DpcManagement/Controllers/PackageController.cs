﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DpcManagement.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DpcManagement.Controllers
{
    public class PackageController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();

        public PackageController(IConfiguration config, ILogger<HomeController> logger)
        {
            _config = config;
            _logger = logger;
            apiClient.SetAddress(_config["AppSettings:ServerApiDpc"]);
        }
        [HttpGet]
        //[Authorize]
        public  IActionResult Index()
        {

            var str = apiClient.Get("api/package", "");
            var packages = JsonConvert.DeserializeObject<List<Package>>(str);
            return View(packages);
        }
        [HttpPost]
        public IActionResult Index(Package package, IFormFile file1)
        {
            if (file1?.Length>0)
            {
                package.DateCreate = DateTime.Now;
                package.IsDelete = false;
                using (var ms = new MemoryStream())
                {
                    file1.CopyTo(ms);
                    var fileBytes = ms.ToArray();
                    var t = apiClient.PostFile("api/package/create"
                                                    , fileBytes, file1.FileName
                                                    , "file"
                                                    , package
                                                    , "strPackage");
                  
                }
            }
            return  RedirectToAction("Index"); 
        }
        [HttpPost]
        public IActionResult Export(Package package)
        {
            if (package.PackageId!=0)
            {
                var res = apiClient.PostPrc("api/Package/Export", package);

            }
            return RedirectToAction("Index");
        }
    }
}