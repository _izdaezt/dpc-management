﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Task = DpcManagement.Models.Task;

namespace DpcManagement.Controllers
{
    public class TaskController : Controller
    {
        private readonly ILogger<TaskController> _logger;
        private readonly IConfiguration _config;
        BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();

        public TaskController(IConfiguration config, ILogger<TaskController> logger)
        {
            _config = config;
            _logger = logger;
            apiClient.SetAddress(_config["AppSettings:ServerApiDpc"]);
        }
        [HttpGet]
        public IActionResult Index(long packageId)
        {
            var str = apiClient.Get($"api/task/Package/{packageId}", "");
            var listTask2 = JsonConvert.DeserializeObject<List<Task>>(str);
            return View(listTask2);
        }
        [HttpGet]
        public async Task<FileResult> ExportPdfNote(long taskId)
        {
            var file = apiClient.GetFile($"api/task/ExportPdfNote/{taskId}", "");
            var stream = await file.Content.ReadAsStreamAsync();
            await using MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            return File(ms.ToArray(), file.Content.Headers.ContentType.ToString(), file.Content.Headers.ContentDisposition.FileNameStar);
        }
        [HttpGet]
        public async Task<FileResult> ExportPdfTag(long taskId)
        {
            var file = apiClient.GetFile($"api/task/ExportPdfTag/{taskId}", "");
            var stream = await file.Content.ReadAsStreamAsync();
            await using MemoryStream ms = new MemoryStream();
            stream.CopyTo(ms);
            return File(ms.ToArray(), file.Content.Headers.ContentType.ToString(), file.Content.Headers.ContentDisposition.FileNameStar);
        }
        
    }
}