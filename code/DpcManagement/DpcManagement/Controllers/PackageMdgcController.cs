﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DpcManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Task = DpcManagement.Models.Task;

namespace DpcManagement.Controllers
{
    public class PackageMdgcController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();

        public string GetFirstClaim(string typeClaim)
        {
            var identity = (System.Security.Claims.ClaimsIdentity)HttpContext.User.Identity;

            var claim = identity.Claims.FirstOrDefault(c => c.Type == typeClaim);
            return claim == null ? "" : claim.Value;
        }
        public List<Claim> GetValueClaims(string typeClaim)
        {
            var identity = (System.Security.Claims.ClaimsIdentity)HttpContext.User.Identity;

            var claim = identity.Claims.Where(c => c.Type == typeClaim).ToList();
            return claim;
        }
        public PackageMdgcController(IConfiguration config, ILogger<HomeController> logger)
        {
            _config = config;
            _logger = logger;
            apiClient.SetAddress(_config["AppSettings:ServerApiDpc"]);
        }

        [HttpGet]
        public IActionResult Index()
        {
            var accName = GetFirstClaim("Name");//"namnn14";
            var res = apiClient.Get($"api/task/ViewTaskMdgc/{accName}", "");
            var listTask = JsonConvert.DeserializeObject<List<Models.Task>>(res);
            return View(listTask);
        }
        [HttpPost]
        public IActionResult Regirtry()
        {
            var accName = GetFirstClaim("Name");//"namnn14";
            var res = apiClient.Get($"api/task/RegistryMdgc/{accName}", "");

            return RedirectToAction("Index", "PackageMdgc");
        }

        [HttpGet]
        public IActionResult ViewTag(long taskId)
        {
            var accName = GetFirstClaim("Name");//"namnn14";
            var res = apiClient.Get($"api/task/GetTask/{taskId}", "");
            var listTask = JsonConvert.DeserializeObject<Models.Task>(res);
            return View(listTask);

        }
        [HttpGet]
        public IActionResult Tagging(long taskId)
        {
            var res = apiClient.Get($"api/task/gettask/{taskId}", "");
            var task = JsonConvert.DeserializeObject<Models.Task>(res);
            return View(task);

        }

        [HttpPost]
        public IActionResult SubmitTag(List<ImageTask> listImageTag, long taskId)
        {
            var accName = GetFirstClaim("Name");
            var imageTags = listImageTag.Select(x => new ImageTagEntry()
            {
                ImageId = (long)x.ImageId,
                Value = x.Image.Value,
                AccName = accName,
                Duration = x.Image.Duration
            }).ToList();
            var rep = apiClient.PostPrc($"api/Task/SubmitTag/{taskId}", imageTags);
            if (rep.status)
            {
                return Json(new
                {
                    newUrl = Url.Action("Index", "PackageMdgc") //Payment as Action; Process as Controller
                });
            }
            else
            {
                return Json(new
                {
                    newUrl = Url.Action("Tagging", "PackageMdgc", new { taskId = taskId }) //Payment as Action; Process as Controller
                });
            }


        }
    }
}