﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DpcManagement.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DpcManagement.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();

        public LoginController(IConfiguration config, ILogger<HomeController> logger)
        {
            _config = config;
            _logger = logger;
            apiClient.SetAddress(_config["AppSettings:ServerApiDpc"]);
        }
        // GET: Login
        [HttpGet]
        public ActionResult Index(string RequestPath = null)
        {
            ViewData["RequestPath"] = RequestPath;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Index(Account acc, string RequestPath = null)
        {
            try
            {
                //_logger.LogDebug("Login port "+JsonConvert.SerializeObject(acc));
                var token = apiClient.PostPrc("api/auth/getToken", acc);
                //_logger.LogDebug("Login port success"+JsonConvert.SerializeObject(token));
                if (token.status)
                {
                    var login = JsonConvert.DeserializeObject<LoginToken>(token.result);
                    // create claims
                    List<Claim> claims = new List<Claim>
                        {
                            new Claim("Name", login.Login),
                            new Claim("Token", login.IdTokenManagement),
                            new Claim("Role","admin"),
                           // new Claim(ClaimTypes.Role,"user")
                        };

                    // create identity
                    ClaimsIdentity identity = new ClaimsIdentity(claims, "cookie");

                    // create principal
                    ClaimsPrincipal principal = new ClaimsPrincipal(identity);

                    // sign-in
                    await HttpContext.SignInAsync(
                            scheme: "SecurityScheme",
                            principal: principal,
                            properties: new AuthenticationProperties
                            {
                                //IsPersistent = true, // for 'remember me' feature
                                //ExpiresUtc = DateTime.UtcNow.AddMinutes(1)
                            });
                    if (RequestPath==null)
                    {
                        return RedirectToAction("User", "Home");
                    }
                    return Redirect(RequestPath ?? "/");
                    
                }
                else
                {

                    ModelState.AddModelError("", "Tên đăng nhập hoăc mật khẩu không đúng.");
                }
                return View();
            }
            catch (Exception e)
            {
                ModelState.AddModelError("", "Kiểm tra lại đường truyền.");
                return View();
            }
        }

        public async Task<IActionResult> Logout(string requestPath)
        {
            await HttpContext.SignOutAsync(
                    scheme: "SecurityScheme");

            return RedirectToAction("Index");
        }
    }
}