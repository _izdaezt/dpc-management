﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DpcManagement.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace DpcManagement.Controllers
{
    public class ImageTaskController : Controller
    {
        private readonly ILogger<ImageTaskController> _logger;
        private readonly IConfiguration _config;
        BPOApiClient.Core.ApiClient apiClient = new BPOApiClient.Core.ApiClient();

        public ImageTaskController(IConfiguration config, ILogger<ImageTaskController> logger)
        {
            _config = config;
            _logger = logger;
            apiClient.SetAddress(_config["AppSettings:ServerApiDpc"]);
        }
        [HttpGet]
        public IActionResult Index(int packageId)
        {
            ViewData["packageId"] = packageId;
            var str = apiClient.Get("api/Package/ReadImage","packageId=" + packageId);
            var listImage = JsonConvert.DeserializeObject<List<Image>>(str);
            return View(listImage);
        }
        [HttpPost]
        public IActionResult Index(List<Image> listImage)
        {
            apiClient.PostPrc("api/ImageTask/Create", listImage);
           // ViewData["packageId"] = packageId;
            //var str = apiClient.Get("api/Package/ReadImage", "packageId=" + packageId);
            //var listImage = JsonConvert.DeserializeObject<List<Image>>(str);
            return RedirectToAction("Index","Package");
        }
    }
}