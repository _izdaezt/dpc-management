﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class ImageTagEntry
    {
        public long ImageId { get; set; }
        public string AccName { get; set; }
        public string Value { get; set; }
        public int? Duration { get; set; }
        public DateTime? DateCreate { get; set; }

        public virtual Image Image { get; set; }
    }
}
