﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class Image
    {
        public Image()
        {
            ImageTask = new HashSet<ImageTask>();
        }

        public long ImageId { get; set; }
        public long? PackageId { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public DateTime? DateCreate { get; set; }
        public  string ContentTask { get; set; }
        public string Value { get; set; }
        public int? Duration { get; set; }
        public virtual ImageTagEntry ImageTagEntry { get; set; }
        public virtual Package Package { get; set; }
        public virtual ICollection<ImageTask> ImageTask { get; set; }
    }
}
