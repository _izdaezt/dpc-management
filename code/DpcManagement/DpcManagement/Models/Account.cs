﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class Account
    {
        public Account()
        {
            AccountRole = new HashSet<AccountRole>();
        }

        public long AccId { get; set; }
        public string AccName { get; set; }
        public string FullName { get; set; }
        public string PathAvatar { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AccUpdate { get; set; }
        public string PassWord { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<AccountRole> AccountRole { get; set; }
    }
}
