﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class AccountRole
    {
        public long AccountRoleId { get; set; }
        public long? AccId { get; set; }
        public long? RoleId { get; set; }
        public DateTime? DateCreate { get; set; }
        public string AccUpdate { get; set; }

        public virtual Account Acc { get; set; }
        public virtual Role Role { get; set; }
    }
}
