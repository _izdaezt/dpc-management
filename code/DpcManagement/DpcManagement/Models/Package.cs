﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class Package
    {
        public Package()
        {
            Image = new HashSet<Image>();
        }

        public long PackageId { get; set; }
        public long? ProjectId { get; set; }
        public string PackageName { get; set; }
        public string FilePath { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateDeadLine { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AccUpdate { get; set; }
        public bool? IsDelete { get; set; }

        public virtual PurchaseOrder Project { get; set; }
        public virtual ICollection<Image> Image { get; set; }
    }
}
