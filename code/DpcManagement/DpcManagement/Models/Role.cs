﻿using System;
using System.Collections.Generic;

namespace DpcManagement.Models
{
    public partial class Role
    {
        public Role()
        {
            AccountRole = new HashSet<AccountRole>();
        }

        public long RoleId { get; set; }
        public string RoleName { get; set; }
        public string Description { get; set; }
        public DateTime? DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string AccUpdate { get; set; }
        public bool? IsDelete { get; set; }

        public virtual ICollection<AccountRole> AccountRole { get; set; }
    }
}
